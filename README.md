# marketplace

To run the server:  
1. Install dependencies: /server/api $ pip install -r requirements.txt  
2. Make migrations: /server/api $ ./manage.py makemigrations  
3. Apply migrations: /server/api $ ./manage.py migrate  
4. Start server: /server/api $ ./manage.py runserver  