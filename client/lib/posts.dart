import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:marketplace/post.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

class PostsScreen extends StatefulWidget{
  const PostsScreen({Key? key}) : super(key: key);

  @override
  PostsScreenState createState() => PostsScreenState();
}

class PostsScreenState extends State<PostsScreen>{
  final TextEditingController filterController = TextEditingController();

  PostsScreenState(){
    filterController.addListener(() {
      if (filterController.text.isEmpty){
        setState(() {
          searchText = "";
          filteredPosts = posts;
        });
      } else {
        setState(() {
          searchText = filterController.text;
        });
      }
    });
  }

  String searchText = "";
  List posts = [];
  List filteredPosts = [];
  final queryPosts = """
    query products{
      products{
        id
        name
        price
        description
        image
        seller {
          id
          name
          phone
          image
        }
      }
    }
  """;

  getPosts() async {
    var httpDestination = HttpLink('http://10.0.2.2:8000/graphql');

    var client = GraphQLClient(
        link: httpDestination,
        cache: GraphQLCache(
              store: InMemoryStore()
          ),
    );

    var result = await client.query(
      QueryOptions(
          document: gql(queryPosts),
      )
    );

    setState(() {
      posts = result.data?['products'];
      filteredPosts = posts;
    });
  }

  @override
  void initState(){
    super.initState();
    getPosts();
  }

  Widget postsListBuilder(){
    if (searchText.isNotEmpty){
      List tempCollection = [];
      // print("FilteredPosts = $filteredPosts");

      for (int post = 0; post < filteredPosts.length; post++){
        if (filteredPosts[post]['name'].toLowerCase().contains(searchText.toLowerCase())){
          tempCollection.add(filteredPosts[post]);
        }
      }
      filteredPosts = tempCollection;
    }

    return ListView.builder(
      itemCount: filteredPosts.length,
      itemBuilder: (context, index) {

        return GestureDetector(
          onTap: (){
            final navigator = Navigator.of(context);
            final route = MaterialPageRoute(
              builder: (context) => PostScreen(
                  data: PostScreenArgs(
                      name: filteredPosts[index]['name'],
                      description: posts[index]['description'],
                      image: filteredPosts[index]['image'],
                      price: filteredPosts[index]['price'],
                      seller: filteredPosts[index]['seller']
                  )
              ),
            );
            navigator.push(route);
          },
          child: Container(
            child: Column(
              children: [
                Row(
                  children: [
                    Image.network(filteredPosts[index]['image'], height: 80, width: 80),
                    Expanded(
                      flex: 10,
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                        child: Text(
                          filteredPosts[index]['name'],
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 0,
                      child: Text(
                        "${filteredPosts[index]['price']}\$",
                        style: const TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ],
                ),
                // Row(
                //   children: [
                //     Text(
                //       "${posts[key]['description']}",
                //       overflow: TextOverflow.ellipsis,
                //     ),
                //   ]
                // ),
              ],
            ),
            padding: const EdgeInsets.all(10.0),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context){
    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(
              title: TextField(
                controller: filterController,
                decoration: const InputDecoration(
                  prefixIcon: Icon(Icons.search),
                  hintText: "Search product"
                ),
              )
          ),
          body: postsListBuilder()
      ),
    );
  }
}