
// ignore_for_file: prefer_const_constructors

import 'dart:collection';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:marketplace/posts.dart';


void main() {
  runApp(
    App(),
  );
}

class App extends StatelessWidget{
  const App({
    Key? key
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: PostsScreen(),
    );
  }}
