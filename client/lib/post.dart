import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:url_launcher/url_launcher.dart';

class PostScreenArgs {
  final String name;
  final double price;
  final String image;
  final String description;
  final Map seller;

  PostScreenArgs({
    required this.name,
    required this.price,
    required this.image,
    required this.description,
    required this.seller,
  });
}

class PostScreen extends StatelessWidget{
  const PostScreen(
      {
        Key? key,
        required this.data,
      }) : super(key: key);

  final PostScreenArgs data;

  @override
  Widget build(BuildContext context){
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text("Post page"),
        ),
        body: Padding(
          padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
          child: Column(
            children: [
              Image.network(data.image, height: 240, width: 240),
              Row(
                children: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(0, 10, 0, 5),
                      child: Text(
                        data.name,
                        style: const TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w300
                        ),
                      ),
                    ),
                  ),
                  Text(
                    "\$${data.price}",
                    style: const TextStyle(
                        fontWeight: FontWeight.bold
                    ),
                  ),
                ],
              ),

              
              Expanded(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: Text(
                        data.description,
                        style: const TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w400
                        ),
                      ),
                    ),
                  ]
                ),
              ),

              Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 0, 10, 0),
                    child: ElevatedButton(
                      onPressed: () async {
                        try {
                          launch('tel://${data.seller['phone']}');
                        } catch(e){
                          print("Error that occurred: $e");
                        }
                      },
                      child: const Text('Call')
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Align(
                        alignment: Alignment.bottomLeft,
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
                          child: Text(
                            data.seller['name'],
                            style: const TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.bold
                            )
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(0, 5, 0, 10),
                        child: Text(
                          "+${data.seller['phone']}",
                          style: const TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.bold
                          )
                        ),
                      ),
                    ]
                  ),
                ]
              ),
            ],
          ),
        ),
      )
    );
  }
}