import graphene
from graphene_django import DjangoObjectType
from .models import *


class ProductType(DjangoObjectType):
    class Meta:
        model = Products
        fields = (
            'id',
            'name',
            'price',
            'description',
            'image',
            'seller'
        )


class SellerType(DjangoObjectType):
    class Meta:
        model = Sellers
        fields = (
            'id',
            'name',
            'phone',
            'image'
        )


class Query(graphene.ObjectType):
    products = graphene.List(ProductType)

    def resolve_products(root, info, **kwargs):
        return Products.objects.all()
    

schema = graphene.Schema(query=Query)
