from django.urls import path
from .views import *
from graphene_django.views import GraphQLView
from django.views.decorators.csrf import csrf_exempt
from .schema import *

urlpatterns = [
    path('', homePage, name="home"),
    path('item', itemPage, name="item"),
    path('graphql', csrf_exempt(GraphQLView.as_view(graphiql=True, schema=schema)))
]